//
//  ProfileItemView.swift
//  mubarak
//
//  Created by Zhuxian Quan on 3/13/18.
//  Copyright © 2018 Zhuxian Quan. All rights reserved.
//

import UIKit

class ProfileItemView: NibView {

    var title: String?
    var value: String?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        titleLabel.text = title
        valueLabel.text = value
    }
    
    func setValues(title: String?, value: String) {
        self.title = title
        self.value = value
        setup()
    }
    
    
}
