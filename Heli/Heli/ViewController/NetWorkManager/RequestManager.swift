//
//  RequestManager.swift
//  Bebarobeshoor
//
//  Created by Developer on 13/02/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import JHSpinner

let BASE_URL  = "" // dev //
//let BASE_URL  = "" // Producation //

let delegate = UIApplication.shared.delegate as! HeliAppDelegate
let isLive = true

class RequestManager : NSObject {
    static var singleton = RequestManager()
    
    func requestGET(requestURL : String, showLoader : Bool, successBlock:@escaping ((_ response : NSDictionary)->Void), failureBlock:@escaping ((_ response : NSDictionary?, _ error : Error?) -> Void)) {

        let window = delegate.window?.rootViewController
        
        let spinner = JHSpinnerView.showOnView((window?.view)!, spinnerColor:UIColor.red, overlay:.roundedSquare, overlayColor:UIColor.black.withAlphaComponent(0.6), fullCycleTime:2.55, text:nil)
        window?.view.addSubview(spinner)
       
        Alamofire.request(requestURL, method: .post, parameters:nil, encoding: JSONEncoding.default, headers: [:])
            .responseJSON { response in
                
                spinner.dismiss()

                switch response.result {
                    case .success(let response):
                            print("response :-----> ",response)
                            successBlock(response as! NSDictionary)
                case .failure(let error):
                        print("Request failed with error: \(error)")
                        failureBlock(nil, error)
                }
            }
        }
}



