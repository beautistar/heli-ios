//
//  ShowNameSMSCell.swift
//  Heli
//
//  Created by Yin on 2018/4/3.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

protocol SwipeDelegate {
    func swipeActionOnSMSCellRight(cell:ShowNameSMSCell)
    func swipeActionOnSMSCellLeft(cell:ShowNameSMSCell)
}

class ShowNameSMSCell: UITableViewCell {

    @IBOutlet weak var ibSMSCellBgView: UIView!
    @IBOutlet weak var ibLeftConstraints: NSLayoutConstraint!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var ibRightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var ibFuckConstraints: NSLayoutConstraint!
    @IBOutlet weak var ibRightToLeftSMSLblName: UILabel!
    @IBOutlet weak var ibLeftToRightSMSLblName: UILabel!
    
    @IBOutlet weak var imvCheck: UIImageView!
    @IBOutlet weak var lblTName: UILabel!
    @IBOutlet weak var imvSocialMedia: UIImageView!
    
    var delegate:SwipeDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeGestureAction))
        swipeLeft.direction = .left
        self.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeGestureAction))
        swipeRight.direction = .right
        self.addGestureRecognizer(swipeRight)
        
    }
    
    @objc func swipeGestureAction(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right")
            if let delegate = self.delegate {
                delegate.swipeActionOnSMSCellRight(cell: self)
            }
            
        } else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left")
            if let delegate = self.delegate {
                delegate.swipeActionOnSMSCellLeft(cell: self)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    

}
