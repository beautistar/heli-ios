//
//  ShowNameCell.swift
//  Heli
//
//  Created by Yin on 2018/4/3.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol PinchActionDelegate {
    func pinchAction(_ indexPath: IndexPath)
    func swipeActionRight(cell:ShowNameCell)
    func swipeActionLeft(cell:ShowNameCell)
}

class ShowNameCell: UITableViewCell {

    @IBOutlet weak var ibRightConstraints: NSLayoutConstraint!
    @IBOutlet weak var ibleftConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var ibFuckingAssLogic: NSLayoutConstraint!
    @IBOutlet weak var ibLeftFuckLogic: NSLayoutConstraint!
    @IBOutlet weak var imvCheck: UIImageView!
    @IBOutlet weak var lblTName: UILabel!
    @IBOutlet weak var imvSocialMedia: UIImageView!
    
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var ibCellBgView: UIView!
    @IBOutlet weak var ibRightToLeftShowLblName: UILabel!
    @IBOutlet weak var ibLeftToRightShowLblName: UILabel!
    
    @IBOutlet weak var ibFackingSocialMedia: UIImageView!
    @IBOutlet weak var ibFuckingLblName: UILabel!
    var delegate: PinchActionDelegate?
    
    var indexPath: IndexPath!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if let recognizers = self.gestureRecognizers {
            for recognizer in recognizers {
                if recognizer.isKind(of: UIPinchGestureRecognizer.self) {
                    recognizer.addTarget(self, action: #selector(pinchAction))
                    return
                }
            }
        }
        let recognizer = UIPinchGestureRecognizer()
        recognizer.addTarget(self, action: #selector(pinchAction(_:)))
        
        self.addGestureRecognizer(recognizer)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeGestureAction))
        swipeLeft.direction = .left
        self.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeGestureAction))
        swipeRight.direction = .right
        self.addGestureRecognizer(swipeRight)
        
    }
    
    @objc func swipeGestureAction(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right")
            if let delegate = self.delegate {
                delegate.swipeActionRight(cell: self)
            }
           
        } else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left")
            if let delegate = self.delegate {
                delegate.swipeActionLeft(cell: self)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func pinchAction(_ gestureRecognizer: UIPinchGestureRecognizer) {
        print("seleted row = ")
        
//        let originalTransform = self.transform
//        let scaledTransform = originalTransform.scaledBy(x: 0.2, y: 0.2)
//        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: -250.0)
//        UIView.animate(withDuration: 0.7, animations: {
//            self.transform = scaledAndTranslatedTransform
//        })
        
        print(self.indexPath.row)
//        if gestureRecognizer.scale > 1.5 {
//            delegate?.pinchAction(self.indexPath)
//            gestureRecognizer.scale = 1
//        }
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
