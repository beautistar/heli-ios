//
//  HoliBaseVC.swift
//  Holi
//
//  Created by praveen on 2/19/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit
import JHSpinner

class HeliBaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
 
    }
  
 
}

@IBDesignable extension UILabel {
    
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}
@IBDesignable extension UIView {
    
    @IBInspectable var brColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var brWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var corRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}
