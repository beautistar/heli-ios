//
//  BrowseCollectionCell.swift
//  Holi
//
//  Created by praveen on 2/21/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

protocol BrowseCellDelegate: class {
    func didTapOnLiveLiveStreams(cell:BrowseCollectionCell)
    func didTapOnOldFavorites(cell:BrowseCollectionCell)
    func didTapOnChannels(cell:BrowseCollectionCell)
    func didTapOnShortStories(cell:BrowseCollectionCell)
    func didTapOnFeatureShows(cell:BrowseCollectionCell)
    func didTapOnLiveEvents(cell:BrowseCollectionCell)
    
    
}

class BrowseCollectionCell: UICollectionViewCell {
    
    weak var delegate: BrowseCellDelegate?
    
    @IBAction func didTapOnLiveLiveStreams(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.didTapOnLiveLiveStreams(cell: self)
        }
    }
    @IBAction func didTapOnOldFavorites(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.didTapOnOldFavorites(cell: self)
        }
    }
    
    @IBAction func didTapOnChannels(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.didTapOnChannels(cell: self)
        }
    }
    @IBAction func didTapOnShortStories(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.didTapOnShortStories(cell: self)
        }
    }
    @IBAction func didTapOnFeatureShows(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.didTapOnFeatureShows(cell: self)
        }
    }
    @IBAction func didTapOnLiveEvents(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.didTapOnLiveEvents(cell: self)
        }
    }
}
