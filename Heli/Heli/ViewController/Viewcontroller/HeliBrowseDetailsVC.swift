//
//  BrowseDetailsVC.swift
//  Holi
//
//  Created by praveen on 2/22/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class HeliBrowseDetailsVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate {

    var vcType:String!
    var items: [String] = ["1","2","3", "4","5","6","7","8","9", "10", "11", "12"]
 
    var removeItems: [Int] = [1,4,7,1,3,4,4,0,1,2]
    var removeCount:Int = 0
    
    var isRemoved: Bool = false
    var swipeleft: Bool = true
    var swipeIndexPath: IndexPath!
    @IBOutlet weak var tblDetails: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = vcType
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isRemoved = false
    }
 
    @IBAction func didTapOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    
    }
    
    private var selectedIndexPath: IndexPath?
    
    @IBAction func pinchZoom(_ sender: UIPinchGestureRecognizer) {
      
        switch sender.state {
        case .began:
            break
        case .ended:
            // Here is time of ending gesture.
            
            if sender.scale > 1 {
                print("pinch out")
                if items.count < 10 {
                    if removeCount == 1 {
                        removeCount = removeCount-1
                        print("pinch In-1")
                        tblDetails.beginUpdates()
                        let indexs:NSArray = [NSIndexPath(item:1, section: 0),
                                              NSIndexPath(item:5, section: 0),
                                              NSIndexPath(item:9, section: 0)]
                        removeItems.insert(1, at: 0)
                        removeItems.insert(4, at: 1)
                        removeItems.insert(7, at: 2)
                        items.insert("2", at: 1)
                        items.insert("6", at: 5)
                        items.insert("10", at: 9)
                        tblDetails.insertRows(at: indexs as! [IndexPath], with: .automatic)
                        tblDetails.endUpdates()

                    }
                    if removeCount == 2 {
                        removeCount = removeCount-1
                        tblDetails.beginUpdates()
                        let indexs:NSArray = [NSIndexPath(item:1, section: 0),
                                              NSIndexPath(item:4, section: 0),
                                              NSIndexPath(item:6, section: 0),
                                              NSIndexPath(item:7, section: 0)]
                        removeItems.insert(1, at: 0)
                        removeItems.insert(3, at: 1)
                        removeItems.insert(4, at: 2)
                        removeItems.insert(4, at: 3)

                        items.insert("3", at: 1)
                        items.insert("7", at: 4)
                        items.insert("9", at: 6)
                        items.insert("11", at: 7)
                        
                        tblDetails.insertRows(at: indexs as! [IndexPath], with: .automatic)
                        tblDetails.endUpdates()
                    }
                    if removeCount == 3 {
                        removeCount = removeCount-1
                        tblDetails.beginUpdates()
                        let indexs:NSArray = [NSIndexPath(item:0, section: 0),
                                              NSIndexPath(item:1, section: 0),
                                              NSIndexPath(item:2, section: 0)]
                        removeItems.insert(0, at: 0)
                        removeItems.insert(1, at: 1)
                        removeItems.insert(2, at: 2)
                        
                        items.insert("1", at: 0)
                        items.insert("5", at: 2)
                        items.insert("12", at: 4)
                        tblDetails.insertRows(at: indexs as! [IndexPath], with: .automatic)
                        tblDetails.endUpdates()
                        tblDetails.reloadRows(at: tblDetails.indexPathsForVisibleRows!, with: .automatic)
                    }
                }
            } else if sender.scale < 1 {
                print("pinch In")
               
                if items.count > 3 {
                    removeCount = removeCount+1
                    if removeCount != 0 {
                        tblDetails.beginUpdates()
                        let indexs:NSArray = [NSIndexPath(item:removeItems[0], section: 0),
                                              NSIndexPath(item:removeItems[1], section: 0),
                                              NSIndexPath(item:removeItems[2], section: 0)]
                        items.remove(at: removeItems[0])
                        items.remove(at: removeItems[1])
                        items.remove(at: removeItems[2])
                        removeItems.remove(at: 2)
                        removeItems.remove(at: 1)
                        removeItems.remove(at: 0)
                        tblDetails.deleteRows(at: indexs as! [IndexPath], with: .automatic)
                        tblDetails.endUpdates()
                        print(items)
                        print(removeItems)
                    }
                    if removeCount == 2 {
                        tblDetails.beginUpdates()
                        let indexs:NSArray = [NSIndexPath(item:removeItems[0], section: 0)]
                        items.remove(at: removeItems[0])
                        removeItems.remove(at: 0)
                        tblDetails.deleteRows(at: indexs as! [IndexPath], with: .automatic)
                        tblDetails.endUpdates()
                    }
                    if removeCount == 3 {
                        tblDetails.reloadRows(at: tblDetails.indexPathsForVisibleRows!, with: .automatic)
                    }
                }
            }
            break
        case .changed:
            
            break
        default:
            break
        }
    }
    
    

    // MARK:  UITableView
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if items[indexPath.row] == "3" || items[indexPath.row] == "9" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowNameSMSCell") as! ShowNameSMSCell
            cell.lblTemp.text = items[indexPath.row]
            cell.ibLeftConstraints.constant = 20
            cell.ibRightConstraints.constant = 15
             cell.ibFuckConstraints.constant = 15
            cell.delegate = self
            
            if items[indexPath.row] == "3"  {
                cell.imvCheck.isHidden = true
                cell.lblTName.isHidden = true
                cell.imvSocialMedia.image = #imageLiteral(resourceName: "facebook")
            } else {
                cell.imvCheck.isHidden = false
                cell.lblTName.isHidden = false
                cell.imvSocialMedia.image = #imageLiteral(resourceName: "twitter")
            }
  
            if indexPath.row == 2 { cell.ibRightToLeftSMSLblName.text = "5" }
            if indexPath.row == 8 { cell.ibRightToLeftSMSLblName.text = "11" }
            if indexPath.row == 8 { cell.ibLeftToRightSMSLblName.text = "2" }
            
            return cell
        } else {
            let cellIdentifier = "ShowNameCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ShowNameCell
            cell.lblTemp.text =  items[indexPath.row] //"\(indexPath.row+1)"
            cell.indexPath = indexPath
            cell.delegate = self
            cell.ibleftConstraints.constant = 20
            cell.ibRightConstraints.constant = 15
            cell.ibFuckingAssLogic.constant = 20
            cell.ibLeftFuckLogic.constant = 20
            
            if items[indexPath.row] == "2" || items[indexPath.row] == "5" || items[indexPath.row] == "10" || items[indexPath.row] == "12" {
                cell.imvCheck.isHidden = true
                cell.lblTName.isHidden = true
                cell.imvSocialMedia.image = #imageLiteral(resourceName: "facebook")
            } else {
                cell.imvCheck.isHidden = false
                cell.lblTName.isHidden = false
                cell.imvSocialMedia.image = #imageLiteral(resourceName: "twitter")
            }
            
            if indexPath.row == 11 {
                cell.ibFackingSocialMedia.image = #imageLiteral(resourceName: "twitter")
            }
            
            
            if indexPath.row == 4 { cell.ibFuckingLblName.text = "3" }
            if indexPath.row == 10 { cell.ibFuckingLblName.text = "9" }
            
            if indexPath.row == 1 { cell.ibRightToLeftShowLblName.text = "9" }
            if indexPath.row == 4 { cell.ibRightToLeftShowLblName.text = "10" }
            if indexPath.row == 5 { cell.ibRightToLeftShowLblName.text = "12" }

            if indexPath.row == 4 { cell.ibLeftToRightShowLblName.text = "3" }
            if indexPath.row == 9 { cell.ibLeftToRightShowLblName.text = "5" }
            if indexPath.row == 10 { cell.ibLeftToRightShowLblName.text = "9"}
            if indexPath.row == 11 { cell.ibLeftToRightShowLblName.text = "6"}
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    // Determine whether a given row is eligible for reordering or not.
    private func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: IndexPath!,_: IndexPath!) -> Bool{
        return false;
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
         return false;
    }
    
    // Process the row move. This means updating the data model to correct the item indices.
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath){
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle{
       return .delete;
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return items.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        if direction == .leftToRight {
            print("leftToRight")
            swipeleft = false
            let view = MyView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 120))
            view.lblTemp.text = "\(2)"
            return [view]
        }
        else if direction == .rightToLeft {
            print("rightToLeft")
            swipeleft = true
            let view = MyView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 120))
            view.lblTemp.text = "\(8)"
            return [view]
        }
        return nil
    }
    
    func swipeTableCellWillEndSwiping(_ cell: MGSwipeTableCell) {
    }
    
    func swipeTableCellWillBeginSwiping(_ cell: MGSwipeTableCell) {
        //print("begin")
        self.perform(#selector(gotoScroll), with: nil, afterDelay: 1.5)
        
    }
    
    @objc func gotoScroll() {
        if swipeleft {
            print("left")
            tblDetails.setContentOffset(CGPoint(x: 0, y: 120*7), animated: true)
        } else {
            print("right")
            tblDetails.setContentOffset(CGPoint(x: 0, y: 120), animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension HeliBrowseDetailsVC: PinchActionDelegate {
    func swipeActionRight(cell: ShowNameCell) {
            print("swipeActionRight")
         let idx:IndexPath = tblDetails.indexPath(for: cell)!
        if idx.row == 0 || idx.row == 1 || idx.row == 2 || idx.row == 3 || idx.row == 5 || idx.row == 6 || idx.row == 7 {
            return;
        }
        
            UIView.animate(withDuration: Double(1.0), animations: {
                if idx.row == 4 || idx.row == 10 {
                    cell.ibLeftFuckLogic.constant = -cell.ibCellBgView.frame.width
                }else{
                    cell.ibRightConstraints.constant = -cell.ibCellBgView.frame.width
                }
                cell.layoutIfNeeded()
            }, completion: { (finished: Bool) in
                var y:Int = 0
                if idx.row == 4 { y = 120*2 }
                if idx.row == 8 { y = 120 }
                if idx.row == 9 { y = 120*4 }
                if idx.row == 10 { y = 120*8 }
                if idx.row == 11 { y = 120*5 }
                
                if idx.row == 4 || idx.row == 10 {
                    cell.ibLeftFuckLogic.constant = 15
                }
                
                if y != 0 {
                    self.tblDetails.setContentOffset(CGPoint(x: 0, y: y), animated: true)
                }
            })
       self.swipeIndexPath = nil
    }
    
    func swipeActionLeft(cell: ShowNameCell) {
         print("swipeActionLeft")
          let idx:IndexPath = tblDetails.indexPath(for: cell)!
        if idx.row == 0 || idx.row == 3 || idx.row == 6 || idx.row == 7 || idx.row == 9 || idx.row == 10 || idx.row == 11 {
            return;
        }
        
      
        self.swipeIndexPath = idx
        print(idx)
        UIView.animate(withDuration: Double(1.0), animations: {
            if idx.row == 1{
                cell.ibFuckingAssLogic.constant = -cell.ibCellBgView.frame.width
            }else{
                cell.ibleftConstraints.constant = -cell.ibCellBgView.frame.width
            }
          cell.layoutIfNeeded()
        }, completion: { (finished: Bool) in
            var y:Int = 0
            if idx.row == 1 { y = 120*8 }
            if idx.row == 2 { y = 120*4 }
            if idx.row == 4 { y = 120*9 }
            if idx.row == 5 { y = 120*11 }
            if idx.row == 8 { y = 120*10 }
            if y != 0 {
                self.tblDetails.setContentOffset(CGPoint(x: 0, y: y), animated: true)
            }
        })
    }
    
    func pinchAction(_ indexPath: IndexPath) {
        //write code for delete action
        self.items.remove(at: indexPath.row)
        self.tblDetails.reloadData()
        
    }
}
extension HeliBrowseDetailsVC: SwipeDelegate {
    func swipeActionOnSMSCellRight(cell: ShowNameSMSCell) {
        print("swipeActionOnSMSCellRight")
          let idx:IndexPath = tblDetails.indexPath(for: cell)!
        if idx.row == 0 || idx.row == 1 || idx.row == 2 || idx.row == 3 || idx.row == 5 || idx.row == 6 || idx.row == 7 {
            return;
        }
            UIView.animate(withDuration: Double(1.0), animations: {
                if idx.row == 8 {
                    cell.ibFuckConstraints.constant = -cell.ibSMSCellBgView.frame.width
                }else {
                    cell.ibLeftConstraints.constant = -cell.ibSMSCellBgView.frame.width
                }
                cell.layoutIfNeeded()
            }, completion: { (finished: Bool) in
                var y:Int = 0
                if idx.row == 4 { y = 120*2 }
                if idx.row == 8 { y = 120 }
                if idx.row == 9 { y = 120*4 }
                if idx.row == 10 { y = 120*8 }
                if idx.row == 11 { y = 120*5 }
                if y != 0 {
                    self.tblDetails.setContentOffset(CGPoint(x: 0, y: y), animated: true)
                }
            })
        self.swipeIndexPath = nil
    }
    
    func swipeActionOnSMSCellLeft(cell: ShowNameSMSCell) {
        print("swipeActionOnSMSCellLeft")
        let idx:IndexPath = tblDetails.indexPath(for: cell)!
        if idx.row == 0 || idx.row == 3 || idx.row == 6 || idx.row == 7 || idx.row == 9 || idx.row == 10 || idx.row == 11 {
            return;
        }
        self.swipeIndexPath = idx
        UIView.animate(withDuration: Double(1.0), animations: {
            cell.ibRightConstraints.constant = -cell.ibSMSCellBgView.frame.width
            cell.layoutIfNeeded()
        }, completion: { (finished: Bool) in
            var y:Int = 0
            if idx.row == 1 { y = 120*8 }
            if idx.row == 2 { y = 120*4 }
            if idx.row == 4 { y = 120*9 }
            if idx.row == 5 { y = 120*11 }
            if idx.row == 8 { y = 120*10 }
            if y != 0 {
                self.tblDetails.setContentOffset(CGPoint(x: 0, y: y), animated: true)
            }
        })
    }
}

//ibFuckConstraints
