//
//  HoliSearchVC.swift
//  Holi
//
//  Created by praveen on 2/19/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliSearchVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK:  UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "HomeCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! HomeCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0{
            return 3
        }else if section == 1{
            return 3
        }
        return 5
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        returnedView.backgroundColor = .clear
        let label = UILabel(frame: CGRect(x: 10, y: 7, width: 120, height: 25))
        label.textColor = .white
        label.textAlignment = .center
        label.cornerRadius = 12.5
        label.backgroundColor = UIColor(red:(173.0/255.0) , green:(22.0/255.0) , blue:(87.0/255.0) , alpha:1 )
        if section == 0{
            label.text = "Feature Shows"
        }else if section == 1{
            label.text = "Short Stories"
        }else if section == 2 {
            label.text = "Live Events"
        } else {
            label.text = "Channels"
        }
        label.font = label.font.withSize(13)
        
        //Underline
        let underlineLabel = UILabel(frame: CGRect(x: 90, y: 7+25-1, width: self.view.bounds.width-110, height: 1))
        underlineLabel.backgroundColor = UIColor(red:(173.0/255.0) , green:(22.0/255.0) , blue:(87.0/255.0) , alpha:1 )
        
        returnedView.addSubview(label)
        returnedView.addSubview(underlineLabel)
        return returnedView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let postVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HeliPostDetailsVC")
        self.navigationController?.pushViewController(postVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
}
