//
//  HoliSettingVC.swift
//  Holi
//
//  Created by praveen on 2/19/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliSettingVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource  {

    override func viewDidLoad() {
        super.viewDidLoad()
 
    }
    
    // MARK:  UITableView
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cellIdentifier = "Setting"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SettingCell
            return cell
        }else if indexPath.row == 1{
            let cellIdentifier = "SocialMedia"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SocialMediaCell
            return cell
        }else{
            let cellIdentifier = "About"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AboutAppCell
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0{
            return 150
        }else if indexPath.row == 1{
            return 180
        }else{
            return 300
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }

    @IBAction func didTapOnBackBtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
