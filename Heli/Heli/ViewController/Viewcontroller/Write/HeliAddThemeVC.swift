//
//  HeliAddThemeVC.swift
//  Heli
//
//  Created by praveen on 3/4/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliAddThemeVC: HeliBaseVC, UITableViewDataSource, UITableViewDelegate {
    
    var themes = ["Theme1", "Goal Theme", "Theme2", "Red Card Theme", "Orange Theme", "Blue Theme", "purple Theme"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "HashtagCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! HashtagCell
        cell.label.text = themes[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return themes.count
    }
    @IBAction func didTapOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
 

}
