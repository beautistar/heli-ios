//
//  HeliChannelNameVC.swift
//  Holi
//
//  Created by praveen on 2/24/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliChannelNameVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK:  UITableView
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "ChannelNameCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ChannelNameCell
        switch indexPath.row {
        case 0:
            cell.imvSocial.image = #imageLiteral(resourceName: "twitter")
        case 1:
            cell.imvSocial.image = #imageLiteral(resourceName: "google")
        case 2:
            cell.imvSocial.image = #imageLiteral(resourceName: "spotify")
        default:
            cell.imvSocial.image = #imageLiteral(resourceName: "facebook")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
    }
    @IBAction func didTapOnBackBtn(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
