//
//  HeliShowNameVC.swift
//  Holi
//
//  Created by praveen on 2/24/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliShowNameVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource  {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func didTapOnBckBtn(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapOnDoneButton(_ sender: Any) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(3) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:  UITableView
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row % 4 == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowNameSMSCell") as! ShowNameSMSCell
            return cell
        } else {
            let cellIdentifier = "ShowNameCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ShowNameCell
            if indexPath.row % 4 == 0 {
                cell.imvCheck.isHidden = true
                cell.lblTName.isHidden = true
                cell.imvSocialMedia.image = #imageLiteral(resourceName: "facebook")
            } else {
                cell.imvCheck.isHidden = false
                cell.lblTName.isHidden = false
                cell.imvSocialMedia.image = #imageLiteral(resourceName: "twitter")
            }
            return cell
        }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
}
