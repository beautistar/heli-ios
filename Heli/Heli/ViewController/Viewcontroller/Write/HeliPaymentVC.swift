//
//  HeliPaymentVC.swift
//  Holi
//
//  Created by praveen on 2/24/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliPaymentVC: HeliBaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func didTapOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapOnConfirmBtn(_ sender: Any) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(3) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    
}
