//
//  HeliAddHashtagsVC.swift
//  Holi
//
//  Created by praveen on 2/24/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliAddHashtagsVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    var hashtags = ["#football", "#derby", "#supersunday", "#goal", "#university", "#philosophy", "#correspondent"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "HashtagCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! HashtagCell
        cell.label.text = hashtags[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return hashtags.count
    }
    @IBAction func didTapOnBackBtn(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
