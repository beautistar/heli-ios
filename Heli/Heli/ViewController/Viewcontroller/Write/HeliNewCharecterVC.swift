//
//  HeliNewCharecterVC.swift
//  Heli
//
//  Created by praveen on 2/25/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliNewCharecterVC: HeliBaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didTapOnCancelBtn(_ sender: Any) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(3) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    @IBAction func didTapOnSaveBtn(_ sender: Any) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(3) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
