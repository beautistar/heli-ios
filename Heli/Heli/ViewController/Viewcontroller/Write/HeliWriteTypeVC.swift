//
//  HeliWriteTypeVC.swift
//  Holi
//
//  Created by praveen on 2/24/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliWriteTypeVC: HeliBaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOnBackBtn(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {        
        
        if segue.identifier == "SegueShortStory" {
            let destVC = segue.destination as! HeliFeatureShowsVC
            destVC.titleName = "Short Story"
        } else if segue.identifier == "SegueFeatureShow" {
            let destVC = segue.destination as! HeliFeatureShowsVC
            destVC.titleName = "Feature Show"
        }
    }

}
