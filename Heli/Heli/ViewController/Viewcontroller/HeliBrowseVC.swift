//
//  HoliBrowseVC.swift
//  Holi
//
//  Created by praveen on 2/19/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliBrowseVC: HeliBaseVC, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, BrowseCellDelegate {
    
   @IBOutlet weak var collectionView: UICollectionView!
    
    var viewType:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewType = ""

        self.collectionView.register(HeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader , withReuseIdentifier: "reuseIdentifier")
 
    }
    
     // MARK:  UICollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath :IndexPath) -> UICollectionViewCell{
        if indexPath.row == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"Browse", for: indexPath) as! BrowseCollectionCell
            cell.delegate = self
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"Genres", for: indexPath) as! GenresCollectionCell
            return cell
        }
        

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if indexPath.row == 0{
             return CGSize(width:collectionView.frame.width-10, height: 230.0)
        }else{
             return CGSize(width: 85.0, height: 85.0)
        }
     }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 14
    }
    
    // MARK:  BrowseCell Delegate
    func didTapOnLiveLiveStreams(cell:BrowseCollectionCell){
        viewType = "Live Streams"
        self.performSegue(withIdentifier: "Details", sender: nil)
    }
    
    func didTapOnOldFavorites(cell:BrowseCollectionCell){
        viewType = "Favorites"
        self.performSegue(withIdentifier: "Details", sender: nil)
    }
    
    func didTapOnChannels(cell:BrowseCollectionCell){
        viewType = "Channels"
        self.performSegue(withIdentifier: "Details", sender: nil)
    }
    
    func didTapOnShortStories(cell:BrowseCollectionCell){
        viewType = "Short Stories"
        self.performSegue(withIdentifier: "Details", sender: nil)
    }
    
    func didTapOnFeatureShows(cell:BrowseCollectionCell){
        viewType = "Feature Shows"
        self.performSegue(withIdentifier: "Details", sender: nil)
    }
    func didTapOnLiveEvents(cell:BrowseCollectionCell){
        viewType = "Live Events"
        self.performSegue(withIdentifier: "Details", sender: nil)
    }
       
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? HeliBrowseDetailsVC
        vc?.vcType = viewType
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
