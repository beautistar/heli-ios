//
//  HoliHomeVC.swift
//  Holi
//
//  Created by praveen on 2/19/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliHomeVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource {

    var showTypes = "";
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RequestManager.singleton.requestGET(requestURL:"", showLoader: true, successBlock: { (response : NSDictionary) in
            
        }) { (response : NSDictionary?, error : Error?) in
            
        }
    }
     // MARK:  UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if (indexPath.section == 1 && indexPath.row == 3) || (indexPath.section == 2 && indexPath.row == 5) {
            
            var cellIdentifier:String!
            cellIdentifier = "FooterCell"
            
            var cell : UITableViewCell!
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            }
            cell.textLabel?.textColor = .white
            cell.textLabel?.textAlignment = .left

            cell.textLabel?.font = cell.textLabel?.font.withSize(12)
            if indexPath.section == 1 {
                cell.textLabel?.text = "See all upcoming shows"
            } else if indexPath.section == 2 {
                cell.textLabel?.text = "See all recently followed"
            }
            return cell
            
        } else {
            let cellIdentifier = "HomeCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! HomeCell
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1 {
            return 4
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == 1 && indexPath.row == 3) || (indexPath.section == 2 && indexPath.row == 5) {
            return 25
        }
        return 90
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        returnedView.backgroundColor = .clear
        let label = UILabel(frame: CGRect(x: 10, y: 7, width: 120, height: 25))
        label.textColor = .white
        label.textAlignment = .center
        label.cornerRadius = 12.5
        label.backgroundColor = UIColor(red:(173.0/255.0) , green:(22.0/255.0) , blue:(87.0/255.0) , alpha:1 )
        
        //Underline
        let underlineLabel = UILabel(frame: CGRect(x: 90, y: 7+25-1, width: self.view.bounds.width-110, height: 1))
        underlineLabel.backgroundColor = UIColor(red:(173.0/255.0) , green:(22.0/255.0) , blue:(87.0/255.0) , alpha:1 )
        
        if section == 0{
             label.text = "Coming Soon"
        }else  if section == 1{
             label.text = "Recently Followed"
        }else if section == 2 {
            label.text = "Features Show"
        }
        label.font = label.font.withSize(13)
        returnedView.addSubview(label)
        returnedView.addSubview(underlineLabel)
        return returnedView
    }
    
    
 
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            showTypes = "Coming Soon"
        }else  if indexPath.section == 1{
            showTypes = "Recently Followed"
        }else if indexPath.section == 2 {
            showTypes = "Features Show"
        }
        
        let postVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HeliBrowseDetailsVC") as! HeliBrowseDetailsVC
        postVC.vcType = showTypes
        self.navigationController?.pushViewController(postVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
}
