//
//  BrowseDetailsVC.swift
//  Holi
//
//  Created by praveen on 2/22/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class BrowseDetailsVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("dddd")
    }
 

    // MARK:  UITableView
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row % 4 == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowNameSMSCell") as! ShowNameSMSCell
            return cell
        } else {
            let cellIdentifier = "ShowNameCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ShowNameCell
            if indexPath.row % 4 == 0 {
                cell.imvCheck.isHidden = true
                cell.lblTName.isHidden = true
                cell.imvSocialMedia.image = #imageLiteral(resourceName: "facebook")
            } else {
                cell.imvCheck.isHidden = false
                cell.lblTName.isHidden = false
                cell.imvSocialMedia.image = #imageLiteral(resourceName: "twitter")
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
}
