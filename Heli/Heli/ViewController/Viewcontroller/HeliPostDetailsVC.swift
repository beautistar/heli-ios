//
//  HeliPostDetailsVC.swift
//  Heli
//
//  Created by praveen on 3/22/18.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class HeliPostDetailsVC: HeliBaseVC, UITableViewDelegate, UITableViewDataSource  {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
 
    
    @IBAction func didTapOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cellIdentifier:String!
        if indexPath.row == 0 && indexPath.section == 0{
            cellIdentifier = "CellPost1"
        }else{
            cellIdentifier = "CellPost2"
        }
     
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.selectionStyle = .none
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0 && indexPath.section == 0{
            return 130
        }else{
            return 75
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        returnedView.backgroundColor = .clear
        let label = UILabel(frame: CGRect(x: 10, y: 7, width: 100, height: 23))
        label.textColor = .white
        label.textAlignment = .center
        label.cornerRadius = 12.5
        label.backgroundColor = UIColor(red:(173.0/255.0) , green:(22.0/255.0) , blue:(87.0/255.0) , alpha:1 )
        
        //Underline
        let underlineLabel = UILabel(frame: CGRect(x: 90, y: 7+23-1, width: self.view.bounds.width-110, height: 1))
        underlineLabel.backgroundColor = UIColor(red:(173.0/255.0) , green:(22.0/255.0) , blue:(87.0/255.0) , alpha:1 )
        
        
        if section == 0 {
            label.text = "Discription"
        }else if section == 1{
            label.text = "Posts"
        }else if section == 2 {
            label.text = "Tweets"
        } else {
            label.text = "Message"
        }
        label.font = label.font.withSize(13)
        returnedView.addSubview(label)
        returnedView.addSubview(underlineLabel)
        return returnedView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
