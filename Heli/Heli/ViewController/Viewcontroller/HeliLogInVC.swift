//
//  HoliLogInVC.swift
//  Holi
//
//  Created by praveen on 2/19/18.
//  Copyright © 2018 dating. All rights reserved.
//

import UIKit

class HeliLogInVC: HeliBaseVC {

    @IBOutlet weak var ibPasswordTxt: UITextField!
    @IBOutlet weak var ibNameTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapOnSignInBtn(_ sender: Any) {
        if ibNameTxt.text == "" ||  ibNameTxt.text == nil {
            return
        }
        
        if ibPasswordTxt.text == "" ||  ibPasswordTxt.text == nil {
            return
        }

        RequestManager.singleton.requestGET(requestURL:"", showLoader: true, successBlock: { (response : NSDictionary) in
          
            self.performSegue(withIdentifier: "TabView", sender: nil)
            
        }) { (response : NSDictionary?, error : Error?) in
              self.performSegue(withIdentifier: "TabView", sender: nil)
        }
    }
 
    @IBAction func didTapOnTwitterBtn(_ sender: Any) {
    }
    
    @IBAction func didTapOngoogleBtn(_ sender: Any) {
    }
    
    @IBAction func didTapOnFaceBookbtn(_ sender: Any) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
}
